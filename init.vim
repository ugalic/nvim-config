" These are for using minpac
set number
set relativenumber
set spell spelllang=en_us
set shell=/bin/bash

packadd minpac
call minpac#init({'verbose': 3})
call minpac#add('mhartington/oceanic-next')

" Adding Themes
if (has("termguicolors"))
 set termguicolors
endif

" Theme
syntax enable
colorscheme OceanicNext

if &diff
colorscheme industry
endif

call minpac#add('mileszs/ack.vim')
call minpac#add('elixir-editors/vim-elixir')
"call minpac#add('neoclide/coc.nvim',{'rev': 'release'})
"call minpac#add('neoclide/coc.nvim', {'rev':'705f94fdd3e9919b6f3c69e4dd44bf3bb5167f60'})
"call minpac#add('neoclide/coc.nvim', {'rev':'6242534f300920296ca27bbecd9d13a0aebbcdbe6242534f300920296ca27bbecd9d13a0aebbcdbe'})
"call minpac#add('neoclide/coc-highlight')
call minpac#add('neoclide/coc.nvim', {'rev': '047a87b01d7d2df2ee1f08ef988ef419051778c1'})
call minpac#add('Chiel92/vim-autoformat')
call minpac#add('airblade/vim-gitgutter')
call minpac#add('vim-airline/vim-airline')
call minpac#add('tpope/vim-fugitive')
call minpac#add('leafoftree/vim-vue-plugin')
call minpac#add('posva/vim-vue')
call minpac#add('leafgarland/typescript-vim')
call minpac#add('junegunn/fzf')
call minpac#add('junegunn/fzf.vim')

"call minpac#add('junegunn/goyo.vim')
"call minpac#add('junegunn/limelight.vim')

" Coc.vim START
" if hidden is not set, TextEdit might fail.
set hidden

" Some servers have issues with backup files 
set nobackup
set nowritebackup

" Better display for messages
set cmdheight=2
"
" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c
"
" always show signcolumns
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction
"
" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
" CrashinwithCocActionnotfond
" autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json,elixir setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Use `:Format` to format current buffer
"command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
"command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
"command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')
" 
	""\ 'coc-snippets',
let g:coc_global_extensions = [
	\ 'coc-pairs',
	\ 'coc-tsserver',
	\ 'coc-json',
	\ 'coc-prettier',
	\ 'coc-elixir',
	\ ]


"Coc.vim END

"vim-airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'jsformatter'


" set number "disabled for now
let mapleader = ","

nnoremap <C-p> :<C-u>FZF<CR>

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25

" disable standart status line --INSERT --
" when using lightline
set noshowmode

" vim vue plugin
let g:vim_vue_plugin_load_full_syntax = 1
let g:vim_vue_plugin_use_typescript = 1


" Put these lines at the very end of your vimrc file.

" load all plugins now.
" plugins need to be added to runtimepath before helptags can be generated.
packloadall
" load all of the helptags now, after plugins have been loaded.
" all messages and errors will be ignored.
silent! helptags ALL
autocmd Filetype gitcommit setlocal spell textwidth=72
